package rubin.recyclerviewitemanimation;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Rubin on 11/26/2015.
 */
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static int ANIMATED_ITEM_COUNT = 0;

    private Context context;
    private int lastAnimatedPosition = -1;
    private int itemCount = 0;

    public RecyclerViewAdapter(Context context) {
        this.context = context;
        ANIMATED_ITEM_COUNT = Utils.getScreenHeight(context) / Utils.dpToPx(72);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler, parent, false);
        return new CellFeedViewHolder(view);
    }


    private void runEnterAnimation(View view, int position) {
        if (position >= ANIMATED_ITEM_COUNT - 1) {
            return;
        }

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(Utils.getScreenHeight(context));
            view.animate()
                    .translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        runEnterAnimation(viewHolder.itemView, position);
        CellFeedViewHolder holder = (CellFeedViewHolder) viewHolder;
        holder.textView.setText("Holder " + position);
    }

    @Override
    public int getItemCount() {
        return itemCount;
    }

    public static class CellFeedViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.textView)
        TextView textView;

        public CellFeedViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    public void updateItems() {
        itemCount = 20;
        notifyDataSetChanged();
    }
}
